>>>配送方法テーブル
CREATE TABLE m_delivery_method (
id int PRIMARY KEY,
name varchar(20) NOT NULL,
price int NOT NULL
);
INSERT INTO m_delivery_method(id, name, price)VALUES (1, '通常配送', 0),(2, 'クール便', 500);

>>>商品テーブル
CREATE TABLE m_item (
id int PRIMARY KEY,
made_id int NOT NULL,
name varchar(20) NOT NULL,
detail text(100) NOT NULL,
price int NOT NULL,
file_name varchar(100) NOT NULL,
create_date date NOT NULL,
update_date date NOT NULL
);

>>>生産地名テーブル
CREATE TABLE m_made (
id int PRIMARY KEY,
name varchar(5) NOT NULL
);
INSERT INTO m_made(id, name)VALUES (1, '北海道'),(2, '青森県'),(3, '岩手県'),(4, '宮城県'),(5, '秋田県'),(6, '山形県'),(7, '福島県'),(8, '茨城県'),(9, '栃木県'),(10, '群馬県'),(11, '埼玉県'),(12, '千葉県'),(13, '東京都'),(14, '神奈川県'),(15, '新潟県'),(16, '富山県'),(17, '石川県'),(18, '福井県'),(19, '山梨県'),(20, '長野県'),(21, '岐阜県'),(22, '静岡県'),(23, '愛知県'),(24, '三重県'),(25, '群馬県'),(26, '京都府'),(27, '大阪府'),(28, '兵庫県'),(29, '奈良県'),(30, '和歌山県'),(31, '鳥取県'),(32, '島根県'),(33, '岡山県'),(34, '広島県'),(35, '山口県'),(36, '徳島県'),(37, '香川県'),(38, '愛媛県'),(39, '高知県'),(40, '福岡県'),(41, '佐賀県'),(42, '長崎県'),(43, '熊本県'),(44, '大分県'),(45, '宮崎県'),(46, '鹿児島県'),(47, '沖縄県');
  
>>>メンバーテーブル
CREATE TABLE t_user (
id int PRIMARY KEY,
name varchar(20) NOT NULL,
postal_code int(10) NOT NULL,
address varchar(50) NOT NULL,
login_id varchar(10) NOT NULL,
login_password varchar(10) NOT NULL,
create_date date NOT NULL
);

>>>t_buyテーブル
CREATE TABLE t_buy (
id int PRIMARY KEY,
user_id int NOT NULL,
total_price int NOT NULL,
delivery_method_id int NOT NULL,
create_date date NOT NULL
);

>>>t_buy_detailテーブル
CREATE TABLE t_buy_detail (
id int PRIMARY KEY,
user_id int NOT NULL,
item_id int NOT NULL
);