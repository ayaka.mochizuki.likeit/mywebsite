<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品マスター登録画面</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>（管理者専用：商品マスター画面）
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="PMTopServlet"><i class="fas fa-caret-right"></i>Home <span
						class="sr-only">(current)</span> </a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMRegisterServlet"><i class="fas fa-caret-right"></i>新規登録</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMDataListServlet"><i class="fas fa-caret-right"></i>商品一覧（更新/削除）</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMUserListServlet"><i class="fas fa-caret-right"></i>メンバー照会
				</a></li>
				<li class="nav-item"><a class="nav-link" href="IndexServlet"><i
						class="fas fa-caret-right"></i>ECサイトに戻る </a></li>
			</ul>
		</div>
	</nav>
	<div class="col-md-10 offset-md-1">
		<div class="container">
			<br>
			<div class="box_big_pmRegist">
				<div class="center-align">
					<h4>
						<i class="fas fa-caret-right"></i> メンバ削除確認 <i
							class="fas fa-caret-left"></i>
					</h4>
				</div>
				<div class="box_small_pmRegist">
					<div class=row>
						<div class="col-md-10 offset-md-1">
							<p class="center-align">
								<label> 本当に「 ${udb.name} 」さんを削除してもよろしいですか？</label>
								<div style="display: inline-flex"><!-- 送信ボタン -->
								<form action="PMUserListServlet" method="get">
									<input type="submit" value="キャンセル" class="btn btn-outline-info">
								</form>
								<form type="hidden" action="PMUserDeleteServlet" method="post">
									<input type="hidden" name="id" value="${udb.id}"> <input
										type="submit" value="削除" class="btn btn-outline-danger">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
