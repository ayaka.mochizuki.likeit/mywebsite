<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee TOP</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="IndexServlet">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、ゲストさん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id>1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="PMTopServlet"><i
							class="fas fa-sitemap"></i>商品マスター画面情報</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link"
					href="UserDataServlet?id=${userInfo.id}"><i
						class="fas fa-user-edit"></i>メンバー情報</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CartServlet?id=${userInfo.id}"><i
						class="fas fa-cart-arrow-down"></i>買い物カゴ</a></li>
				<c:if test="${not empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LogoutServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログアウト </a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LoginServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログイン </a></li>
				</c:if>
			</ul>
		</div>
	</nav>
	<div class="box_small_cart">
		<h4>ショッピングカート</h4>
		${cartActionMessage}
	</div>
	<form action="ItemDelete" method="POST">
		<p class="center-align">
			<button type="submit" name="action" class="btn btn-outline-danger">
				削除 <i class="fas fa-trash-alt"></i>
			</button>
			<button type="button" class="btn btn-outline-info">
				<a href="BuyServlet">レジに進む<i class="fas fa-angle-right"></i>
			</button>
		</p>
		<div class="container">
			<div class="row">
				<c:forEach var="item" items="${cart}" varStatus="status">
					<div class="col-3">
						<div class="card">
							<div class="card-image">
								<a href="ItemServlet?item_id=${item.id}"><img
									src="img/${item.fileName}" alt="${item.madeName}の画像"> </a>
							</div>
							<div class="card-content">
								<span class="card-title"><b>${item.itemName}</b></span>
								<p>「${item.madeName}」${item.price}円 （税込）</p>
								<p>
									<input type="checkbox" id="${status.index}"
										name="delete_item_id_list" value="${item.id}" /> <label
										for="${status.index}">削除</label>
								</p>
							</div>
						</div>
					</div>
					<c:if test="${(status.index+1) % 4 == 0 }">
						<div class="row"></div>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</form>
</body>
</html>