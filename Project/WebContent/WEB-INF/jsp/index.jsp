<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee TOP</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="IndexServlet">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、ゲストさん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id>1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="PMTopServlet"><i
							class="fas fa-sitemap"></i>商品マスター画面</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link"
					href="UserDataServlet?id=${userInfo.id}"><i
						class="fas fa-user-edit"></i>メンバー情報</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CartServlet?id=${userInfo.id}"><i
						class="fas fa-cart-arrow-down"></i>買い物カゴ</a></li>
				<c:if test="${not empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LogoutServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログアウト </a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LoginServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログイン </a></li>
				</c:if>
			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row no-gutters">
			<div class="col-12 col-sm-6 col-md-8">
				<img src="img/index1.jpeg">
			</div>
			<div class="col-6 col-md-4">
				<h4><i class="fas fa-caret-right"></i><i class="fas fa-caret-right"></i>　クラフトビールとは...?</h4>
				<br>
				<p>
					小規模な醸造所がつくる「多様で個性的なビール」を指します。これまでにない多様性と、個性的な味わいやブランドを備えているのが特徴です。<br>
					<br>こちらのサイトでは、日本で醸造された"クラフトビール"を生産地名やキーワードで検索することができます。
				</p>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col">
				<div class="box_big_index">
					<div class="box_small_index">
						<form action="SearchByMadeServlet" method="get"
							name="searchMadeId">
							<i class="fas fa-search"></i><br> <select name="madeId">
								<option value="" selected>都道府県</option>
								<c:forEach var="made" items="${madeList}">
									<option value="${made.id}">${made.name}</option>
								</c:forEach>
							</select><br>
							<button type="submit" class="btn btn-dark">「 生産地名 」 で検索</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="box_big_index">
					<div class="box_small_index">
						<form action="SearchByWordServlet" method="get"
							name="searchWord">
							<i class="fas fa-search"></i><input type="text"
								class="form-control" placeholder="Search" name="searchWord">
							<button type="submit" class="btn btn-dark">「キーワード」 で検索</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<h4><i class="fas fa-caret-right"></i><i class="fas fa-caret-right"></i>　おすすめ商品 pick up</h4>
			<br><br>
		</div>
		<div class="row">
			<c:forEach var="item" items="${itemList}">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="ItemServlet?item_id=${item.id}"><img
								src="img/${item.fileName}" alt="${item.madeName}の画像"></a>
						</div>
						<div class="card-content">
							<span class="card-title"><b>${item.itemName}</b></span>
							<p>「${item.madeName}」${item.price}円 （税込）</p>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>
