<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
</head>
<body>
	<div class="col-md-6 offset-md-3">
		<div class="container">
			<br>
			<h1 class="head">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
			<div class="box_big_error">
				<div class="box_small_error">
					<h5>
						<i class="fas fa-exclamation-triangle"></i> ERROR <i
							class="fas fa-exclamation-triangle"></i>
					</h5>
					<br>
					<h6>
						お探しのページが見つかりません。<br>再度、TOPページよりアクセスをお願いします。
					</h6>
					<br>
					<form>
						<button type="button" class="btn btn-dark">TOPページに戻る</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
