<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee TOP</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="IndexServlet">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="#"><b><i class="fas fa-caret-right"></i>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、ゲストさん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id>1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="PMTopServlet"><i
							class="fas fa-sitemap"></i>商品マスター画面</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link"
					href="UserDataServlet?id=${userInfo.id}"><i
						class="fas fa-user-edit"></i>メンバー情報</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CartServlet?id=${userInfo.id}"><i
						class="fas fa-cart-arrow-down"></i>買い物カゴ</a></li>
				<c:if test="${not empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LogoutServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログアウト </a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LoginServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログイン </a></li>
				</c:if>
			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row">
			<div class="col s5">
				<div class="card">
					<div class="card-image">
						<img src="img/${item.fileName}" alt="${item.itemName}の画像">
					</div>
				</div>
			</div>
			<div class="col s5">
				<h4>
					<i class="fas fa-beer"></i> 商品名<br> <br> ${item.itemName}
				</h4>
				<br>
				<div class="form-row">
					<!-- 生産地名エリア -->
					<div class="form-group col-md-6">
						<h5>
							<label> 生産地：${item.madeName}</label>
						</h5>
					</div>
					<!-- 価格エリア -->
					<div class="form-group col-md-6">
						<h5>
							<label>消費税込：${item.price}円</label>
						</h5>
					</div>
				</div>
				<h4 class="left-align">
					<i class="fas fa-beer"></i> 詳細文
				</h4>
				<p class="left-align">
					<label> ${item.detailSentence}</label>
				<div>
					<br>
					<c:if test="${item.itemCount!=0}">
						<form action="ItemAddServlet" method="POST">
							<input type="hidden" name="item_id" value="${item.id}">
							<button type="submit" class="btn btn-dark">
								買い物カゴに追加 <i class="fas fa-cart-arrow-down"></i>
							</button>
						</form>
					</c:if>
					<c:if test="${item.itemCount==0}">
						<button type="button" class="btn btn-dark">
							申し訳ございません。在庫切れです。</button>
					</c:if><br>
					<br> <label>●在庫本数：${item.itemCount}本</label>
				</div>
				<label>●配送料金<br>　・クール便（1500円）<br>　・通常発送（1000円）
				</label>
			</div>
		</div>
	</div>
</body>
</html>
