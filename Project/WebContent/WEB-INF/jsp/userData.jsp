<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee TOP</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="IndexServlet">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、ゲストさん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id>1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="PMTopServlet"><i
							class="fas fa-sitemap"></i>商品マスター画面情報</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link"
					href="UserDataServlet?id=${userInfo.id}"><i
						class="fas fa-user-edit"></i>メンバー情報</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CartServlet?id=${userInfo.id}"><i
						class="fas fa-cart-arrow-down"></i>買い物カゴ</a></li>
				<c:if test="${not empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LogoutServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログアウト </a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LoginServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログイン </a></li>
				</c:if>
			</ul>
		</div>
	</nav>
<body>
	<div class="col-md-8 offset-md-2">
		<div class="container">
			<br>
			<div class="box_big_data">
				<div class="center-align">
					<h5>
						<i class="fas fa-user-edit"></i>メンバー情報
					</h5>
				</div>
				<div class="box_small_data">
					<h6 class=left-align>ログイン情報確認</h6>
					<div class=row>
						<div class="col-md-8 offset-md-2">
							<!-- ログインID入力エリア -->
							<p class="left-align">
								<i class="fas fa-user-alt"></i> ID
							</p>
							<input type="text" class="form-control" name="login_id"
								placeholder="${userInfo.loginId}"><br>
						</div>
					</div>
				</div>
				<div class="box_small_data">
					<form action="UserDataUpDateServlet" method="post">
						<h6 class=left-align>お客様情報確認（氏名と住所のみ変更可）</h6>
						<br>
						<div class=row>
							<div class="col-md-8 offset-md-2">
								<p class="left-align">
									<i class="fas fa-user-alt"></i> 氏名
								</p>
								<div class="input-group">
									<input type="text" class="form-control" name="user_name"
										value="${userInfo.name}">
								</div>
								<br>
								<!-- パスワード入力エリア -->
								<p class="left-align">
									<i class="fas fa-home"></i>住所
								</p>
								<input type="text" class="form-control" name="user_address"
									value="${userInfo.address}"><br>
								<p class="center-align">
									<!-- 送信ボタン -->
									<input type="hidden" name="UserId" value="${userInfo.id}">
									<button type="submit" class="btn btn-dark">上記内容に更新</button>
									<br>
									<br>※ 更新後は再度ログインを行います。
								</p>
							</div>
						</div>
					</form>
				</div>
				<div class="center-align">
					<form action="UserDataServlet" method="post">
						<input type="hidden" name="UserId" value="${userInfo.id}">
						<button type="submit" class="btn btn-dark">購入履歴を参照</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>