<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>（管理者専用：商品マスター画面）
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="PMTopServlet"><i class="fas fa-caret-right"></i>Home <span
						class="sr-only">(current)</span> </a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMRegisterServlet"><i class="fas fa-caret-right"></i>新規登録</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMDataListServlet"><i class="fas fa-caret-right"></i>商品一覧（更新/削除）</a></li>
				<li class="nav-item"><a class="nav-link" href="PMUserListServlet"><i
						class="fas fa-caret-right"></i>メンバー照会 </a></li>
				<li class="nav-item"><a class="nav-link" href="IndexServlet"><i
						class="fas fa-caret-right"></i>ECサイトに戻る </a></li>
			</ul>
		</div>
	</nav>
	<div class="center-align">
		<h3>
			<i class="fas fa-caret-right"></i> 商品一覧 <i class="fas fa-caret-left"></i>
		</h3>
		<h6>※在庫数が10本未満になると、ボタンが赤く変わります。</h6>

	</div>
	<div class="box_small_pmRegist">
		<!--  購入履歴 -->
		<div class="center-align">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">商品名</th>
						<th scope="col">在庫数</th>
						<th scope="col">生産地</th>
						<th scope="col">税込価格</th>
						<th scope="col">更新日時</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${itemList}">
						<tr>
							<td><b>${item.itemName}</b></td>
							<td><c:if test="${item.itemCount<10}">
									<a class="btn btn-danger"
										href="PMInventoryServlet?id=${item.id}">${item.itemCount}</a>
								</c:if> <c:if test="${item.itemCount>=10}">
									<a class="btn btn-primary"
										href="PMInventoryServlet?id=${item.id}">${item.itemCount}</a>
								</c:if></td>
							<td>${item.madeName}</td>
							<td>${item.price}円</td>
							<td>${item.upDateDate}</td>
							<td><a class="btn btn-primary"
								href="PMDetailServlet?id=${item.id}">詳細</a> <a
								class="btn btn-success" href="PMUpDateServlet?id=${item.id}">更新</a>
								<a class="btn btn-danger" href="PMDeleteServlet?id=${item.id}">削除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
