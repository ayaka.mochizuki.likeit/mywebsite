<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品マスター登録画面</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>（管理者専用：商品マスター画面）
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="PMTopServlet"><i class="fas fa-caret-right"></i>Home <span
						class="sr-only">(current)</span> </a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMRegisterServlet"><i class="fas fa-caret-right"></i>新規登録</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMDataListServlet"><i class="fas fa-caret-right"></i>商品一覧（更新/削除）</a></li>
				<li class="nav-item"><a class="nav-link" href="PMUserListServlet"><i
						class="fas fa-caret-right"></i>メンバー照会 </a></li>
				<li class="nav-item"><a class="nav-link" href="IndexServlet"><i
						class="fas fa-caret-right"></i>ECサイトに戻る </a></li>
			</ul>
		</div>
	</nav>
	<div class="col-md-10 offset-md-1">
		<div class="container">
			<br>
			<div class="box_big_pmRegist">
				<div class="center-align">
					<h4>
						<i class="fas fa-caret-right"></i> 商品情報登録 <i
							class="fas fa-caret-left"></i>
					</h4>
				</div>
				<div class="box_small_pmRegist">
					<div class=row>
						<div class="col-md-10 offset-md-1">
							<!-- 商品名エリア -->
							<p class="left-align">
								<i class="fas fa-beer"></i> 商品名
							</p>
							<p class="left-align">
								<label>『 ${idb.itemName} 』 </label> <br>
							<div class="form-row">
								<!-- 生産地名エリア -->
								<div class="form-group col-md-6">
									<p class="left-align">
										<i class="fas fa-apple-alt"></i> 生産地（県名）： ${idb.madeName}
									</p>
								</div>
								<!-- 価格エリア -->
								<div class="form-group col-md-6">
									<p class="left-align">
										<i class="fas fa-dollar-sign"></i> 税込価格: ${idb.price}円
									</p>
								</div>
							</div>
							<div class="form-row">
								<!-- 商品詳細文エリア -->
								<p class="left-align">
									<i class="fas fa-align-left"></i> 商品詳細文
								</p>
								<p class="left-align">
									<label> ${idb.detailSentence}</label>
							</div>
							<!-- 商品画像エリア -->
							<p class="left-align">
								<i class="far fa-image"></i> 商品画像
							</p>
							<div class="form-group col-md-3">
								<img src="img/${idb.fileName}" alt="${idb.itemName}の画像">
								<p>
									画像ファイル名 <br>${idb.fileName}</p>
							</div>
							<br>
							<div class="form-row">
								<!-- 生産地名エリア -->
								<div class="form-group col-md-6">
									<p class="left-align">
										<i class="far fa-calendar-alt"></i> 更新日時
									</p>
									<p class="left-align">
										<label> ${idb.upDateDate}</label>
								</div>
								<!-- 価格エリア -->
								<div class="form-group col-md-6">
									<p class="left-align">
										<i class="far fa-calendar-alt"></i> 登録日時
									</p>
									<p class="left-align">
										<label> ${idb.createDate}</label>
								</div>
							</div>
							<br>
							<p class="center-align">
								<!-- 送信ボタン -->
								<button type="button" class="btn btn-dark">
									<a href="PMDataListServlet">戻る</a>
								</button>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
