<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee メンバーログイン</title>
<meta name=”description” content=”ログイン画面・クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="col-md-10 offset-md-1">
		<div class="container">
			<br> <a class="navbar-brand" href="IndexServlet">
				<h1 class="head_pm">
					CRAFT-Bee <i class="fas fa-beer"></i>
				</h1>
			</a>
			<div class="box_big">
				<div class="col-md-10 offset-md-1">
					<div class="box_small">
						<h5 class="left-align">● 会員の方</h5>
						<P class="left-align">&nbsp;&nbsp;アカウントをお持ちの方は、こちらからログインしてご利用ください。</P>
						<c:if test="${errMsg != null}">
							<div class="alert alert-danger" role="alert">${errMsg}</div>
						</c:if>
						<form action="LoginServlet" method="post">
							<div class=row>
								<div class="col-md-8 offset-md-2">
									<!-- ログインID入力エリア -->
									<p class="left-align">
										<i class="fas fa-user-alt"></i> ID
									</p>
									<input type="text" class="form-control"
										id="exampleFormControlInput1" placeholder="登録時に入力したログインID"
										name="loginId"><br>
									<!-- パスワード入力エリア -->
									<p class="left-align">
										<i class="fas fa-lock"></i> パスワード
									</p>
									<input type="password" class="form-control"
										id="exampleFormControlInput1" placeholder="登録時に入力したパスワード"
										name="password"><br>
									<p class="center-align">
										<!-- 送信ボタン -->
										<button type="submit" class="btn btn-dark">Sign in</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="box_small">

						<h5 class="left-align">● 新規会員登録</h5>
						<div class=row>
							<div class="col-md-8 offset-md-2">
								<p class="center-align">
									<button type="button" class="btn btn-dark">
										<a href="UserRegisterServlet">新規メンバー登録</a>
									</button>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>