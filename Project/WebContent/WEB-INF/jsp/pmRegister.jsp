<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品マスター登録画面</title>
<meta name=”description”
	content=”都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>（管理者専用：商品マスター画面）
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="PMTopServlet"><i class="fas fa-caret-right"></i>Home <span
						class="sr-only">(current)</span> </a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMRegisterServlet"><i class="fas fa-caret-right"></i>新規登録</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMDataListServlet"><i class="fas fa-caret-right"></i>商品一覧（更新/削除）</a></li>
				<li class="nav-item"><a class="nav-link"
					href="PMUserListServlet"><i class="fas fa-caret-right"></i>メンバー照会
				</a></li>
				<li class="nav-item"><a class="nav-link" href="IndexServlet"><i
						class="fas fa-caret-right"></i>ECサイトに戻る </a></li>
			</ul>
		</div>
	</nav>
	<div class="col-md-10 offset-md-1">
		<div class="container">
			<br>
			<div class="box_big_pmRegist">
				<div class="center-align">
					<h4>
						<i class="fas fa-caret-right"></i> 新規商品登録 <i
							class="fas fa-caret-left"></i>
					</h4>
				</div>
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<div class="box_small_pmRegist">
					<div class=row>
						<div class="col-md-10 offset-md-1">
							<!-- ファイルアップロードの実装含む enctype-->
							<form method="post" action="PMRegisterServlet"
								enctype="multipart/form-data">
								<!-- 商品名エリア -->
								<p class="left-align">
									<i class="fas fa-beer"></i> 商品名
								</p>
								<input type="text" class="form-control"
									id="exampleFormControlInput1" placeholder="常陸野ネスト ホワイトエール 木内酒造"
									name="name"><br>
								<div class="form-row">
									<!-- 在庫数エリア -->
									<div class="form-group col-md-6">
										<p class="left-align">
											<i class="fas fa-dollar-sign"></i> 在庫数
										</p>
										<input type="number" class="form-control" id="inputPassword4"
											placeholder="100" name="itemCount">
									</div>
									<!-- 価格エリア -->
									<div class="form-group col-md-6">
										<p class="left-align">
											<i class="fas fa-dollar-sign"></i> 価格（税込）
										</p>
										<input type="number" class="form-control" id="inputPassword4"
											placeholder="400" name="price">
									</div>
								</div>
								<!-- 生産地名エリア -->
								<div class="form-group col-md-6">
									<p class="left-align">
										<i class="fas fa-apple-alt"></i> 生産地（県名）
									</p>
									<select name="madeId">
										<option value="" selected>都道府県</option>
										<c:forEach var="made" items="${madeList}">
											<option value="${made.id}">${made.name}</option>
										</c:forEach>
									</select>
								</div>
								<!-- 商品詳細文エリア -->
								<p class="left-align">
									<i class="fas fa-align-left"></i> 商品詳細文
								</p>
								<textarea class="form-control" id="exampleFormControlInput1"
									placeholder="奥深い味わいの小麦麦芽とスパイシーなホップ。さらにコリアンダー、オレンジピール、ナツメグ等のスパイスを加えて醸造するベルギーの伝統的ビール、ベルジャンホワイトエールのスタイルで造ったうすにごり小麦ビールです。スパイスの豊かな香りと小麦特有の軽い酸味があいまって様々な料理にも相性よくお飲みいただけます。"
									name="detailSentence"></textarea>
								<br>
								<!-- 商品画像エリア -->
								<p class="left-align">
									<i class="far fa-image"></i> 商品画像
								</p>
								<input type="file" id="exampleFormControlInput1"
									placeholder="●●●●.jpeg" name="file" /><br />
								<!-- 送信ボタン -->
								<p class="center-align">
									<button type="submit" class="btn btn-dark">登録する</button>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
