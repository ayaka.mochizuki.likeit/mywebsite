<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CRAFT-Bee TOP</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="IndexServlet">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、ゲストさん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id>1}">
					<li class="nav-item"><a class="nav-link" href="#"><b>ようこそ、${userInfo.name}さん</b></a></li>
				</c:if>
				<c:if test="${userInfo.id==1}">
					<li class="nav-item"><a class="nav-link" href="PMTopServlet"><i
							class="fas fa-sitemap"></i>商品マスター画面</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link"
					href="UserDataServlet?id=${userInfo.id}"><i
						class="fas fa-user-edit"></i>メンバー情報</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CartServlet?id=${userInfo.id}"><i
						class="fas fa-cart-arrow-down"></i>買い物カゴ</a></li>
				<c:if test="${not empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LogoutServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログアウト </a></li>
				</c:if>
				<c:if test="${empty userInfo.id}">
					<li class="nav-item"><a class="nav-link"
						href="LoginServlet?id=${userInfo.id}"><i
							class="fas fa-sign-out-alt"></i>ログイン </a></li>
				</c:if>
			</ul>
		</div>
	</nav>
	<div class="box_big_pmRegist">
		<div class="center-align">
			<h5>カートアイテム</h5>
		</div>
		<div class="box_small_pmRegist">
			<!--  購入履歴 -->
			<div class="center-align">
				<form action="BuyConfirmServlet" method="post">
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">商品名</th>
								<th scope="col">生産地</th>
								<th scope="col">小計（税込）</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="cartInItem" items="${cart}">
								<tr>
									<td class="center"><b>${cartInItem.itemName}</b></td>
									<td class="center">${cartInItem.madeName}</td>
									<td class="center">${cartInItem.price}円</td>
								</tr>
							</c:forEach>
							<tr>
								<th scope="row"></th>
								<td></td>
								<td></td>
								<td>配送方法選択 : <select name="deliveryId">
										<c:forEach var="dmdb" items="${dmdbList}">
											<option value="${dmdb.id}">${dmdb.deliveryName}</option>
										</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
					<p class="center-align">
						<button class="btn btn-dark" type="submit" name="action">購入確認画面へ</button>
					</p>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
