package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import Beans.MadeinBeans;
import dao.MadeinDao;
import dao.PMItemDao;

@WebServlet("/PMUpDateServlet")
public class PMUpDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// 都道府県情報を取得
		MadeinDao madeinDao = new MadeinDao();
		List<MadeinBeans> madeList = madeinDao.findAllMadein();
		//// リクエストスコープに都道府県情報情報をセット
		request.setAttribute("madeList", madeList);
		// 既存情報を取得
		String id = request.getParameter("id");
		PMItemDao pmid = new PMItemDao();
		ItemDataBeans idb = pmid.findItemById(id);
		request.setAttribute("idb", idb);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmUpDate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String madeId = request.getParameter("madeId");
		String itemName = request.getParameter("itemName");
		String detailSentence = request.getParameter("detailSentence");
		String price = request.getParameter("price");
		String fileName = request.getParameter("fileName");
		if (madeId.equals("") || itemName.equals("") || detailSentence.equals("") || price.equals("")
				|| fileName.equals("")) {
			request.setAttribute("errMsg", "空白のフォームがあります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmRegister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		PMItemDao pmid = new PMItemDao();
		pmid.update(madeId, itemName, detailSentence, price, fileName, id);
		response.sendRedirect("PMDataListServlet");
	}
}
