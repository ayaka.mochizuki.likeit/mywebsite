package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import Beans.MadeinBeans;
import dao.MadeinDao;
import dao.PMItemDao;

/**
 * Servlet implementation class PMRegisterServlet
 */
@WebServlet("/PMRegisterServlet")
@MultipartConfig(location = "/Users/ayataso71/Documents/MyWebSite/Project/WebContent/img", maxFileSize = 1048576)
public class PMRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// 都道府県情報を取得
		MadeinDao madeinDao = new MadeinDao();
		List<MadeinBeans> madeList = madeinDao.findAllMadein();
		// リクエストスコープに都道府県情報情報をセット
		request.setAttribute("madeList", madeList);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmRegister.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String madeId = request.getParameter("madeId");
		String name = request.getParameter("name");
		String detailSentence = request.getParameter("detailSentence");
		String price = request.getParameter("price");
		String itemCount = request.getParameter("itemCount");

		// ファイルアップロードの実装
		Part part = request.getPart("file");
		String fileName = this.getFileName(part);
		part.write(fileName);

		if (madeId.equals("") || name.equals("") || detailSentence.equals("") || price.equals("")
				|| fileName.equals("") || itemCount.equals("")) {
			request.setAttribute("errMsg", "空白のフォームがあります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmRegister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		PMItemDao.insert(madeId, name, detailSentence, price, fileName, itemCount);
		response.sendRedirect("PMTopServlet");
	}

	// ファイルアップロードの実装
	private String getFileName(Part part) {
		String fileName = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				fileName = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
				break;
			}
		}
		return fileName;
	}
}
