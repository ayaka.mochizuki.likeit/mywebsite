package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import Beans.MadeinBeans;
import dao.MadeinDao;
import dao.PMItemDao;

@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			// 都道府県情報を取得
			MadeinDao madeinDao = new MadeinDao();
			List<MadeinBeans> madeList = madeinDao.findAllMadein();
			// リクエストスコープに都道府県情報情報をセット
			request.setAttribute("madeList", madeList);

			// 商品情報を取得
			ArrayList<ItemDataBeans> itemList = PMItemDao.getRandItem(4);
			// リクエストスコープにセット
			request.setAttribute("itemList", itemList);
			// セッションにsearchWordが入っていたら破棄する
			String searchWord = (String) session.getAttribute("searchWord");
			if (searchWord != null) {
				session.removeAttribute("searchWord");
			}

			// フォワード処理
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}
