package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class RegistServlet
 */
@WebServlet("/UserRegisterServlet")
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordVer = request.getParameter("passwordVer");
		String name = request.getParameter("name");
		String address = request.getParameter("address");

		if (loginId.equals("") || password.equals("") || passwordVer.equals("") || name.equals("")
				|| address.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if (!password.equals(passwordVer)) {
			request.setAttribute("errMsg", "入力されているパスワードと確認用パスワードが違います。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao userDao = new UserDao();
		boolean find = userDao.findByLoginId(loginId);
		if (find) {
			request.setAttribute("errMsg", "他のユーザーとログインIDが重複しています。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		userDao.insert(loginId, password, name, address);
		response.sendRedirect("UserRegisterTreseltServlet");
	}
}
