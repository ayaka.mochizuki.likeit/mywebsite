package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import dao.BuyDao;

@WebServlet("/UserBuyDetailServlet")
public class UserBuyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		// ログイン確認!=ならログイン画面に遷移する。
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// 購入日別の詳細情報を取得する。
		String id = request.getParameter("id");
		List<ItemDataBeans> detailList;
		try {
			detailList = BuyDao.getBuyDataBeansByBuyId(id);
			request.setAttribute("detailList", detailList);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// 購入日別の詳細画面にフォワード。
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userBuyDetail.jsp");
		dispatcher.forward(request, response);
	}
}
