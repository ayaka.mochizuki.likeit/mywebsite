package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import dao.BuyDao;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserDataServlet")
public class UserDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();

		// ログイン確認!=ならログイン画面に遷移する。
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// メンバ情報画面にフォワード。
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userData.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String userId = request.getParameter("UserId");
		// 購入履歴取得後、リクエストスコープにユーザ一覧情報をセットする。（例外処理）
		List<BuyDataBeans> buyUserIdList;
		try {
			buyUserIdList = BuyDao.getBuyDataBeansByBuyUserId(userId);
			request.setAttribute("buyUserIdList", buyUserIdList);

			// メンバ情報画面にフォワード。
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userBuyDataList.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
