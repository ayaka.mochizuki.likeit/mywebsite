
package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import dao.PMItemDao;

/**
 * Servlet implementation class PMDetailServlet
 */
@WebServlet("/PMDetailServlet")
public class PMDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// 既存情報を取得
		String id = request.getParameter("id");
		PMItemDao pmid = new PMItemDao();
		ItemDataBeans idb = pmid.findItemById(id);
		request.setAttribute("idb", idb);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmDetail.jsp");
		dispatcher.forward(request, response);
	}
}
