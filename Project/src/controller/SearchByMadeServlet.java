package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import Beans.MadeinBeans;
import dao.MadeinDao;
import dao.PMItemDao;

/**
 * Servlet implementation class SearchByMadeServlet
 */
@WebServlet("/SearchByMadeServlet")
public class SearchByMadeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			// 都道府県情報を取得
			MadeinDao madeinDao = new MadeinDao();
			List<MadeinBeans> madeList = madeinDao.findAllMadein();

			// リクエストスコープに都道府県情報情報をセット
			request.setAttribute("madeList", madeList);

			// 都道府県idを取得
			String madeId = request.getParameter("madeId");

			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("madeId", madeId);

			// 商品リストを取得
			ArrayList<ItemDataBeans> itemList = PMItemDao.getItemsByMadeName(madeId);

			// 表示ページ
			request.setAttribute("itemList", itemList);

			// searchByにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemSearch.jsp");
			dispatcher.forward(request, response);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
