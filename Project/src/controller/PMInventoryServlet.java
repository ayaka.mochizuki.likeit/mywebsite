package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import dao.PMItemDao;

@WebServlet("/PMInventoryServlet")
public class PMInventoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		// ログイン可否判定
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// 商品在庫情報を取得
		String id = request.getParameter("id");
		PMItemDao pmid = new PMItemDao();
		ItemDataBeans item = pmid.findItemById(id);
		request.setAttribute("item", item);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmInventory.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String newItemCount = request.getParameter("itemCount");

		// 在庫追加数のフォーム欄空白判定
		if (newItemCount.equals("")) {
			request.setAttribute("errMsg", "追加在庫数量が入力されておりません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmInventory.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// 在庫＋新規在庫数　追加処理
		PMItemDao pmid = new PMItemDao();
		pmid.updateInventory(newItemCount, id);
		response.sendRedirect("PMDataListServlet");
	}

}
