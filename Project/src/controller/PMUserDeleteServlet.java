package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.UserDataBeans;
import dao.UserDao;

@WebServlet("/PMUserDeleteServlet")
public class PMUserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		UserDataBeans udb = userDao.display(id);
		request.setAttribute("udb", udb);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmUserDelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		userDao.delete(id);
		//user一覧画面へに遷移する。
		response.sendRedirect("PMUserListServlet");
	}

}
