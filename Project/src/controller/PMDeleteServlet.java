package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import dao.PMItemDao;

/**
 * Servlet implementation class PMDeleteServlet
 */
@WebServlet("/PMDeleteServlet")
public class PMDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");
		PMItemDao pmidao = new PMItemDao();
		ItemDataBeans idb = pmidao.display(id);
		request.setAttribute("idb", idb);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pmDelete.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		PMItemDao pmidao = new PMItemDao();
		pmidao.delete(id);
		//item一覧画面へに遷移する。
		response.sendRedirect("PMDataListServlet");
	}
}