package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.DeliveryMethodDataBeans;
import Beans.ItemDataBeans;
import dao.BuyDao;
import dao.DeliveryMethodDao;

@WebServlet("/BuyConfirmServlet")
public class BuyConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		try {
			request.setCharacterEncoding("UTF-8");
			String deliveryId = request.getParameter("deliveryId");

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			DeliveryMethodDao deliveryMethodDao = new DeliveryMethodDao();
			// 選択されたIDをもとに配送方法Beansを取得。
			@SuppressWarnings("static-access")
			DeliveryMethodDataBeans dmd = deliveryMethodDao.getDeliveryMethodDataBeansByID(deliveryId);

			// 買い物かご、取得。
			@SuppressWarnings("unchecked")
			ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			// 合計金額
			int totalPrice = BuyDao.getTotalItemPrice(cartIDBList) + dmd.getPrice();

			BuyDataBeans bdb = new BuyDataBeans();
			//bdb.setUserId((int) session.getAttribute("userId"));
			//bdb.setUserId((Integer) session.getAttribute("userInfo"));
			bdb.setTotalPrice(totalPrice);
			bdb.setDelivertMethodId(dmd.getId());
			bdb.setDeliveryMethodName(dmd.getDeliveryName());
			bdb.setDeliveryMethodPrice(dmd.getPrice());

			// 購入確定で利用
			session.setAttribute("bdb", bdb);
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
