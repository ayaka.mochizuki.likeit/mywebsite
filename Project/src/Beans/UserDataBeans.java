package Beans;

import java.io.Serializable;
import java.sql.Date;

/**
 * ユーザー登録
 */
public class UserDataBeans implements Serializable {
	private String loginId;
	private String password;
	private String name;
	private String address;
	private int id;
	private Date createDate;

	// ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(int id, String loginId, String name, String address, String password) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.address = address;
		this.password = password;
	}

	//user情報更新を行うコンストラクタ
	public UserDataBeans(int id, String name, String address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}

	//userList表示用のコンストラクタ
	public UserDataBeans(int id, String loginId, String name, String address, Date createDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.address = address;
		this.createDate = createDate;
	}

	//userデータ削除画面取得を行うコンストラクタ
		public UserDataBeans(int id, String name) {
			this.id = id;
			this.name = name;
		}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}