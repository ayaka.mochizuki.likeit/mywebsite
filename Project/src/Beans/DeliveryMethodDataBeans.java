package Beans;

import java.io.Serializable;

public class DeliveryMethodDataBeans implements Serializable {
	private int id;
	private String deliveryName;
	private int price;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}