package Beans;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class ItemDataBeans implements Serializable {
	private int id;
	private int madeId;
	private String itemName;
	private String detailSentence;
	private int price;
	private String fileName;
	private Date createDate;
	private Date upDateDate;
	private String madeName;
	private int itemCount;

	public ItemDataBeans(int id, int madeId, String itemName, String detailSentence, int price, String fileName,
			Date createDate, Date upDateDate, String madeName) {
		this.id = id;
		this.madeId = madeId;
		this.itemName = itemName;
		this.detailSentence = detailSentence;
		this.price = price;
		this.fileName = fileName;
		this.createDate = createDate;
		this.upDateDate = upDateDate;
		this.madeName = madeName;
	}

	public ItemDataBeans(int id, int madeId, String itemName, String detailSentence, int price, String fileName,
			Date createDate, Date upDateDate, String madeName, int itemCount) {
		this.id = id;
		this.madeId = madeId;
		this.itemName = itemName;
		this.detailSentence = detailSentence;
		this.price = price;
		this.fileName = fileName;
		this.createDate = createDate;
		this.upDateDate = upDateDate;
		this.madeName = madeName;
		this.itemCount = itemCount;
	}

	public ItemDataBeans(int id, int madeId, String itemName, String detailSentence, int price, String fileName,
			Date upDateDate) {
		this.id = id;
		this.madeId = madeId;
		this.itemName = itemName;
		this.detailSentence = detailSentence;
		this.price = price;
		this.fileName = fileName;
		this.upDateDate = upDateDate;
	}

	public ItemDataBeans(int id1, String itemName) {
		this.id = id1;
		this.itemName = itemName;
	}

	//itemをランダムに取得するコンストラクタ、index画面
	public ItemDataBeans(int id, int madeId, String itemName, String detailSentence, int price, String fileName, String madeName) {
		this.id = id;
		this.madeId = madeId;
		this.madeName = madeName;
		this.itemName = itemName;
		this.detailSentence = detailSentence;
		this.price = price;
		this.fileName = fileName;
	}

	public ItemDataBeans(int id, String madeName, String itemName, String detailSentence, int price, String fileName) {
		this.id = id;
		this.madeName = madeName;
		this.itemName = itemName;
		this.detailSentence = detailSentence;
		this.price = price;
		this.fileName = fileName;
	}

	public ItemDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMadeId() {
		return madeId;
	}

	public void setMadeId(int madeId) {
		this.madeId = madeId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDetailSentence() {
		return detailSentence;
	}

	public void setDetailSentence(String detailSentence) {
		this.detailSentence = detailSentence;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpDateDate() {
		return upDateDate;
	}

	public void setUpDateDate(Date upDateDate) {
		this.upDateDate = upDateDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(upDateDate);
	}

	public String getMadeName() {
		return madeName;
	}

	public void setMadeName(String madeName) {
		this.madeName = madeName;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
}