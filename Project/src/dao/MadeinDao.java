package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.MadeinBeans;
import bese.DBManager;

public class MadeinDao {
	/*
	 * 都道府県LIST取得
	 * */
	public List<MadeinBeans> findAllMadein() {
		Connection conn = null;
		List<MadeinBeans> madeList = new ArrayList<>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM m_made";
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// 結果表に格納されたレコードの内容を
			// MadeinBeansインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("made_name");
				MadeinBeans made = new MadeinBeans(id,name);
				madeList.add(made);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return madeList;
	}
}
