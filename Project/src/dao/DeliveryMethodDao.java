package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Beans.DeliveryMethodDataBeans;
import bese.DBManager;

public class DeliveryMethodDao {
	/*
	 * DBの配送方法を取得する。
	 */
	public static ArrayList<DeliveryMethodDataBeans> getAllDeliveryMethodDataBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM m_delivery_method");
			ResultSet rs = st.executeQuery();
			ArrayList<DeliveryMethodDataBeans> deliveryMethodDataBeansList = new ArrayList<DeliveryMethodDataBeans>();
			while (rs.next()) {
				DeliveryMethodDataBeans dmdb = new DeliveryMethodDataBeans();
				dmdb.setId(rs.getInt("id"));
				dmdb.setDeliveryName(rs.getString("delivery_name"));
				dmdb.setPrice(rs.getInt("price"));
				deliveryMethodDataBeansList.add(dmdb);
			}
			System.out.println("All DeliveryMethodDataBeans has been completed");
			return deliveryMethodDataBeansList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
	 * idで配送方法を取得する
	 */
	public static DeliveryMethodDataBeans getDeliveryMethodDataBeansByID(String deliveryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM m_delivery_method WHERE id = ?");
			st.setString(1, deliveryId);
			ResultSet rs = st.executeQuery();
			DeliveryMethodDataBeans dmdb = new DeliveryMethodDataBeans();
			if (rs.next()) {
				dmdb.setId(rs.getInt("id"));
				dmdb.setDeliveryName(rs.getString("delivery_name"));
				dmdb.setPrice(rs.getInt("price"));
			}
			System.out.println("searching DeliveryMethodDataBeans by DeliveryMethodID has been completed");
			return dmdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}