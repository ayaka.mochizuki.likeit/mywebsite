package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import Beans.UserDataBeans;
import bese.DBManager;

public class UserDao {
	/*
	 *メンバー登録
	 * */
	public void insert(String loginId, String password, String name, String address) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			String hash = hash(password);
			con = DBManager.getConnection();
			String insertSQL = "INSERT INTO t_user(login_id,login_password,name,address,create_date)"
					+ " VALUES (?,?,?,?,NOW())";
			stmt = con.prepareStatement(insertSQL);
			stmt.setString(1, loginId);
			stmt.setString(2, hash);
			stmt.setString(3, name);
			stmt.setString(4, address);
			stmt.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String hash(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}

	/*
	 *LoginId重複検索
	 * */
	public boolean findByLoginId(String loginId) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();
			//確認済みのSQL
			String sql = "SELECT * FROM t_user WHERE login_id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}

	/*
	 *LoginId重複検索のち、すべてのログインデータをセットする。
	 * */
	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			String hash = hash(password);
			// データベースへ接続
			conn = DBManager.getConnection();
			//確認済みのSQL
			String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, hash);
			ResultSet rs = pStmt.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}
			// ログイン成功時の処理		・・・②
			int IdData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String addressData = rs.getString("address");
			String passwordData = rs.getString("login_password");

			return new UserDataBeans(IdData, loginIdData, nameData, addressData, passwordData);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static String getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return (String) null;
			}
			// ログイン成功時の処理		・・・②
			String id = rs.getString("id");
			System.out.println("searching userId by loginId has been completed");
			return id;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザー情報の更新処理を行う。
	 * @return
	 */
	public static UserDataBeans updateUser(String name, String address, String id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name=?, address=? WHERE id=?;");
			st.setString(1, name);
			st.setString(2, address);
			st.setString(3, id);
			st.executeUpdate();
			System.out.println("update has been completed");
			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return null;
	}

	/**
	 * 全てのメンバー情報を取得する
	 * @return
	 */
	public List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM t_user";
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String address = rs.getString("address");
				Date createDate = rs.getDate("create_date");
				UserDataBeans user = new UserDataBeans(id, name, loginId, address, createDate);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
	/*
	 * userデータ削除画面取得
	 */
	public UserDataBeans display(String id) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();
			//確認済みのSQL
			String sql = "SELECT * FROM t_user WHERE id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}
			// ログイン成功時の処理		・・・②
			int id1 = rs.getInt("id");
			String name = rs.getString("name");
			UserDataBeans udb = new UserDataBeans(id1, name);
			return udb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	/*
	 * メンバー削除確定
	 */
	public void delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String deleteSQL = "DELETE FROM t_user WHERE id=?";
			stmt = con.prepareStatement(deleteSQL);
			stmt.setString(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}