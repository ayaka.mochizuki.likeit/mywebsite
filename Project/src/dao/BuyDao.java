package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.ItemDataBeans;
import bese.DBManager;

public class BuyDao {
	/**
	 * 商品の合計金額を算出
	 */
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> cartIDBList) {
		int totalPrice = 0;
		for (ItemDataBeans item : cartIDBList) {
			totalPrice += item.getPrice();
		}
		return totalPrice;
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);
		return test;
	}

	/**
	 * 購入情報登録処理,
	 * BuyResultServletで使用する
	 */
	public static int insertBuy(BuyDataBeans bdb, String userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setString(1, userId);
			st.setInt(2, bdb.getTotalPrice());
			st.setInt(3, bdb.getDelivertMethodId());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @return BuyDataBeans購入情報のデータを持つJavaBeansのリスト
	 */
	public static BuyDataBeans getBuyDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy"
							+ " JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.id = ?");
			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();
			BuyDataBeans bdb = new BuyDataBeans();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setDeliveryMethodName(rs.getString("name"));
			}
			System.out.println("searching BuyDataBeans by buyID has been completed");

			return bdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザー購入IDによる購入情報検索
	 */
	public static List<BuyDataBeans> getBuyDataBeansByBuyUserId(String userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<BuyDataBeans> buyUserIdList = new ArrayList<BuyDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy"
							+ " JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.user_id = ?");
			st.setString(1, userId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodName(rs.getString("delivery_name"));
				bdb.setDeliveryMethodPrice(rs.getInt("m_delivery_method.price"));
				buyUserIdList.add(bdb);
			}
			System.out.println("searching BuyDataBeans by buyID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return buyUserIdList;
	}

	/**
	 * ユーザー購入IDによる購入情報検索(詳細)
	 */
	public static List<ItemDataBeans> getBuyDataBeansByBuyId(String id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<ItemDataBeans> detailList = new ArrayList<ItemDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy_detail "
							+ "JOIN m_item "
							+ "ON t_buy_detail.item_id = m_item.id "
							+ "RIGHT JOIN m_made "
							+ "ON t_buy_detail.id = m_made.id "
							+ "WHERE t_buy_detail.buy_id = ?");
			st.setString(1, id);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ItemDataBeans bdb = new ItemDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setMadeName(rs.getString("made_name"));
				bdb.setItemName(rs.getString("item_name"));
				bdb.setDetailSentence(rs.getString("detail"));
				bdb.setPrice(rs.getInt("price"));
				bdb.setFileName(rs.getString("file_name"));
				detailList.add(bdb);
			}
			System.out.println("searching BuyDataBeans by buyID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return detailList;
	}
}