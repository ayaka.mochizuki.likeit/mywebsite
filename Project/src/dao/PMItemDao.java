package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.ItemDataBeans;
import bese.DBManager;

public class PMItemDao {
	/*
	 *商品情報登録
	 * */
	public static void insert(String madeId, String name, String detailSentence, String price, String fileName, String itemCount) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String insertSQL = "INSERT INTO m_item(made_id,item_name,detail,price,file_name,item_count,create_date,update_date)"
					+ " VALUES (?,?,?,?,?,?,NOW(),NOW())";
			stmt = con.prepareStatement(insertSQL);
			stmt.setString(1, madeId);
			stmt.setString(2, name);
			stmt.setString(3, detailSentence);
			stmt.setString(4, price);
			stmt.setString(5, fileName);
			stmt.setString(6, itemCount);
			stmt.executeUpdate();
			System.out.println("inserting item has been completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 全ての商品情報を取得する
	 * @return
	 */
	public List<ItemDataBeans> findAll() {
		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id ";
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				int madeId = rs.getInt("made_id");
				String itemName = rs.getString("item_name");
				String madeName = rs.getString("made_name");
				String detail = rs.getString("detail");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				Date createDate = rs.getDate("create_date");
				Date upDateDate = rs.getDate("update_date");
				int itemCount = rs.getInt("item_count");
				ItemDataBeans item = new ItemDataBeans(id, madeId, itemName, detail, price, fileName, createDate,
						upDateDate, madeName, itemCount);
				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	/**
	 * 各商品情報を取得、参照する
	 * @return
	 */
	public ItemDataBeans findItemById(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//確認済みのSQL
			String sql = "SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id WHERE m_item.id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}
			// 情報取得成功時の処理		・・・②
			int id2 = rs.getInt("id");
			int madeId = rs.getInt("made_id");
			String itemName = rs.getString("item_name");
			String madeName = rs.getString("made_name");
			String detail = rs.getString("detail");
			int price = rs.getInt("price");
			String fileName = rs.getString("file_name");
			Date createDate = rs.getDate("create_date");
			Date upDateDate = rs.getDate("update_date");
			int itemCount = rs.getInt("item_count");

			return new ItemDataBeans(id2, madeId, itemName, detail, price, fileName, createDate, upDateDate, madeName, itemCount);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/*
	 * 商品情報更新
	 */
	public void update(String madeId, String itemName, String detailSentence, String price, String fileName,
			String id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String updateSQL = "UPDATE m_item SET made_id=?,item_name=?,detail=?,price=?,file_name=?,update_date=now() WHERE id=?";
			stmt = con.prepareStatement(updateSQL);
			stmt.setString(1, madeId);
			stmt.setString(2, itemName);
			stmt.setString(3, detailSentence);
			stmt.setString(4, price);
			stmt.setString(5, fileName);
			stmt.setString(6, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * item削除画面取得
	 */
	public ItemDataBeans display(String id) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();
			//確認済みのSQL
			String sql = "SELECT * FROM m_item WHERE id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();
			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}
			// ログイン成功時の処理		・・・②
			int id1 = rs.getInt("id");
			String itemName = rs.getString("item_name");
			ItemDataBeans idb = new ItemDataBeans(id1, itemName);
			return idb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/*
	 * item削除確定ボタン
	 */
	public void delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String deleteSQL = "DELETE FROM m_item WHERE id=?";
			stmt = con.prepareStatement(deleteSQL);
			stmt.setString(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * 検索画面で商品情報をランダムで4つ取得する。
	 */
	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);
			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setMadeName(rs.getString("made_name"));
				item.setItemName(rs.getString("item_name"));
				item.setDetailSentence(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 */
	public static ItemDataBeans getItemByItemID(String id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id WHERE m_item.id = ?");
			st.setString(1, id);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setMadeName(rs.getString("made_name"));
				item.setItemName(rs.getString("item_name"));
				item.setDetailSentence(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setItemCount(rs.getInt("item_count"));
			}
			System.out.println("searching item by itemID has been completed");
			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 都道府県で、商品検索
	 * @param madeId
	 */
	public static ArrayList<ItemDataBeans> getItemsByMadeName(String madeId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			if (madeId.length() == 0) {
				// 全検索
				st = con.prepareStatement(
						"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id ORDER BY m_item.id ASC");
			} else {
				// 商品名検索
				st = con.prepareStatement(
						"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id WHERE m_made.id = ? ORDER BY m_item.id ASC");
				st.setString(1, madeId);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setMadeName(rs.getString("made_name"));
				item.setItemName(rs.getString("item_name"));
				item.setDetailSentence(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 部分一致で、商品検索
	 * @param madeId
	 */
	public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement(
						"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id ORDER BY m_item.id ASC");
			} else {
				// 商品名検索
				st = con.prepareStatement(
						"SELECT * FROM m_item JOIN m_made ON m_item.made_id = m_made.id WHERE item_name LIKE ? ORDER BY m_item.id ASC");
				st.setString(1, "%" + searchWord + "%");
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setMadeName(rs.getString("made_name"));
				item.setItemName(rs.getString("item_name"));
				item.setDetailSentence(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * 在庫数更新、PMInventoryServlet
	 */
	@SuppressWarnings("resource")
	public void updateInventory(String newItemCount, String id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();

			// 現在庫数取得
			String selectSQL = "SELECT item_count FROM m_item WHERE m_item.id = ?";
			stmt = con.prepareStatement(selectSQL);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();
			// 失敗時の処理		・・・①
			if (!rs.next()) {
				return;
			}
			// 成功時の処理		・・・②
			int itemCount = rs.getInt("item_count");
			int newItemCountInt = Integer.parseInt(newItemCount);
			int totalCount = itemCount + newItemCountInt;

			// 在庫数更新SQL
			String updateSQL = "UPDATE m_item SET item_count=? WHERE id=?";
			stmt = con.prepareStatement(updateSQL);
			stmt.setInt(1, totalCount);
			stmt.setString(2, id);
			stmt.executeUpdate();

			System.out.println("upDate Inventory count has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}