package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import Beans.BuyDetailDataBeans;
import Beans.ItemDataBeans;
import bese.DBManager;

/**
 * Servlet implementation class BuyDetailDao
 */
@WebServlet("/BuyDetailDao")
public class BuyDetailDao {

	/**
	 * 購入詳細登録処理
	 * @param bddb BuyDetailDataBeans
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");

			// 現在庫数参照処理
			String selectSQL = "SELECT item_count FROM m_item WHERE m_item.id = ?";
			st = con.prepareStatement(selectSQL);
			st.setInt(1, bddb.getItemId());
			ResultSet rs = st.executeQuery();

			// 失敗時の処理		・・・①
			if (!rs.next()) {
				return;
			}

			// 成功時の処理		・・・② 現在庫数を変数に追加する
			int itemCount = rs.getInt("item_count");
			itemCount--;

			// 在庫数更新SQL
			String updateSQL = "UPDATE m_item SET item_count=? WHERE id=?";
			st = con.prepareStatement(updateSQL);
			st.setInt(1, itemCount);
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return {BuyDataDetailBeans}
	 * @throws SQLException
	 */
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入詳細情報検索
	 * @param buyId
	 * @return buyDetailItemList ArrayList<ItemDataBeans>
	 *             購入詳細情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_item.id,m_item.name,"
							+ " m_item.price"
							+ " FROM t_buy_detail"
							+ " JOIN m_item"
							+ " ON t_buy_detail.item_id = m_item.id"
							+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();
			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setItemName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				buyDetailItemList.add(idb);
			}
			System.out.println("searching ItemDataBeansList by BuyID has been completed");

			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
