<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light"> <a
		class="navbar-brand" href="#">
		<h1 class="head_pm">
			CRAFT-Bee <i class="fas fa-beer"></i>
		</h1>
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarNav" aria-controls="navbarNav"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="#">メンバー情報<i
					class="fas fa-user-check"></i></a></li>
			<li class="nav-item"><a class="nav-link" href="#">買い物カゴ<i
					class="fas fa-cart-arrow-down"></i></a></li>
			<li class="nav-item"><a class="nav-link" href="#">ログアウト<i
					class="fas fa-sign-out-alt"></i>
			</a></li>
		</ul>
	</div>
	</nav>
	<div class="container">
		<div class="row no-gutters">

			<div class="col-12 col-sm-6 col-md-8">
				<img src="picture4.jpg" />
			</div>
			<div class="col-6 col-md-4">
				<h4>クラフトビールとは?</h4>
				<br>
				<p>
					小規模な醸造所がつくる「多様で個性的なビール」を指します。これまでにない多様性と、個性的な味わいやブランドを備えているのが特徴です。<br>
					<br>こちらのサイトでは、日本で醸造された"クラフトビール"を生産地名やキーワードで検索することができます。
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="box_big_index">
					<div class="box_small_index">
						<form action="ItemSearchResult">
							<i class="fas fa-search"></i><br> <select name="pref_id">
								<option value="" selected>都道府県</option>
								<option value="1">北海道</option>
								<option value="2">青森県</option>
								<option value="3">岩手県</option>
								<option value="4">宮城県</option>
								<option value="5">秋田県</option>
								<option value="6">山形県</option>
								<option value="7">福島県</option>
								<option value="8">茨城県</option>
								<option value="9">栃木県</option>
								<option value="10">群馬県</option>
								<option value="11">埼玉県</option>
								<option value="12">千葉県</option>
								<option value="13">東京都</option>
								<option value="14">神奈川県</option>
								<option value="15">新潟県</option>
								<option value="16">富山県</option>
								<option value="17">石川県</option>
								<option value="18">福井県</option>
								<option value="19">山梨県</option>
								<option value="20">長野県</option>
								<option value="21">岐阜県</option>
								<option value="22">静岡県</option>
								<option value="23">愛知県</option>
								<option value="24">三重県</option>
								<option value="25">滋賀県</option>
								<option value="26">京都府</option>
								<option value="27">大阪府</option>
								<option value="28">兵庫県</option>
								<option value="29">奈良県</option>
								<option value="30">和歌山県</option>
								<option value="31">鳥取県</option>
								<option value="32">島根県</option>
								<option value="33">岡山県</option>
								<option value="34">広島県</option>
								<option value="35">山口県</option>
								<option value="36">徳島県</option>
								<option value="37">香川県</option>
								<option value="38">愛媛県</option>
								<option value="39">高知県</option>
								<option value="40">福岡県</option>
								<option value="41">佐賀県</option>
								<option value="42">長崎県</option>
								<option value="43">熊本県</option>
								<option value="44">大分県</option>
								<option value="45">宮崎県</option>
								<option value="46">鹿児島県</option>
								<option value="47">沖縄県</option>
							</select>
						</form>
						<button type="button" class="btn btn-dark">「 生産地名 」 で検索</button>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="box_big_index">
					<div class="box_small_index">
						<form action="ItemSearchResult">
							<i class="fas fa-search"></i><input type="text"
								class="form-control" placeholder="Search">
							<button type="button" class="btn btn-dark">「キーワード」 で検索</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<h4>おすすめ商品・ピックアップ</h4>
		</div>
		<div class="row">
			<div class="col">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="#"><img src="item1.jpg"></a>
						</div>
						<div class="card-content">
							<span class="card-title">常陸野ネスト ホワイトエール 木内酒造</span>
							<p>（消費税込）400円『茨城県』</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="#"><img src="item1.jpg"></a>
						</div>
						<div class="card-content">
							<span class="card-title">常陸野ネスト ホワイトエール 木内酒造</span>
							<p>（消費税込）400円『茨城県』</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="#"><img src="item1.jpg"></a>
						</div>
						<div class="card-content">
							<span class="card-title">常陸野ネスト ホワイトエール 木内酒造</span>
							<p>（消費税込）400円『茨城県』</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
