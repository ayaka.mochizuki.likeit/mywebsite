<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <meta name=”description” content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。”/><!--メタデスクリプション-->
        <link href="style.css" rel="stylesheet" type="text/css" /><!--CSSの読み込み-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><!--bootstrap-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <h1 class="head_pm">CRAFT-Bee <i class="fas fa-beer"></i></h1>（管理者専用：商品マスター画面）</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"         aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">新規登録</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">商品一覧（詳細/更新/削除）</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ECサイトに戻る </a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="box_big_pmRegist">
            <div class="center-align">
                <h5>新規商品情報登録</h5>
            </div>
            <div class="box_small_pmRegist">
                <!--  購入履歴 -->
                <div class="center-align">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">　商品名　</th>
                                <th scope="col">　生産地(県名)　</th>
                                <th scope="col">　値段(値段)</th>
                                <th scope="col">　更新日時</th>
                                <th scope="col">　</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>オリオン</td>
                                <td>沖縄</td>
                                <td>300円</td>
                                <td>2020年4月11日</td>
                                <td>
                                    <button type="button" class="btn btn-outline-success">詳細</button>
                                    <button type="button" class="btn btn-outline-info">更新</button>
                                    <button type="button" class="btn btn-outline-danger">削除</button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>キリン</td>
                                <td>神奈川</td>
                                <td>300円</td>
                                <td>2020年4月11日</td>
                                <td>
                                    <button type="button" class="btn btn-outline-success">詳細</button>
                                    <button type="button" class="btn btn-outline-info">更新</button>
                                    <button type="button" class="btn btn-outline-danger">削除</button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>アサヒ</td>
                                <td>東京</td>
                                <td>300円</td>
                                <td>2020年4月11日</td>
                                <td>
                                    <button type="button" class="btn btn-outline-success">詳細</button>
                                    <button type="button" class="btn btn-outline-info">更新</button>
                                    <button type="button" class="btn btn-outline-danger">削除</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
