<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <meta name=”description” content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。”/><!--メタデスクリプション-->
        <link href="style.css" rel="stylesheet" type="text/css" /><!--CSSの読み込み-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><!--bootstrap-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <h1 class="head_pm">CRAFT-Bee <i class="fas fa-beer"></i></h1>（管理者専用：商品マスター画面）</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"         aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">新規登録</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">商品一覧（更新/削除）</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ECサイトに戻る </a>
                    </li>
                </ul>
            </div>
        </nav>
        <img src="picture3.jpeg" />
    </body>
</html>
