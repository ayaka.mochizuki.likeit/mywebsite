<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link" href="#">メンバー情報<i
						class="fas fa-user-check"></i></a></li>
				<li class="nav-item"><a class="nav-link" href="#">買い物カゴ<i
						class="fas fa-cart-arrow-down"></i></a></li>
				<li class="nav-item"><a class="nav-link" href="#">ログアウト<i
						class="fas fa-sign-out-alt"></i>
				</a></li>
			</ul>
		</div>
	</nav>
        <div class="col-md-4 offset-md-4">
            <div class="container"><br>
                <div class="box_big_logout">
                    <div class="box_small_logout">
                    <h6>ご購入有難う御座います。</h6>
                        <form>
                            <button type="button" class="btn btn-dark">TOPページに戻る</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
