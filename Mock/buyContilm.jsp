<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta name=”description”
	content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。” />
<!--メタデスクリプション-->
<link href="style.css" rel="stylesheet" type="text/css" />
<!--CSSの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--bootstrap-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<h1 class="head_pm">
				CRAFT-Bee <i class="fas fa-beer"></i>
			</h1>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link" href="#">メンバー情報<i
						class="fas fa-user-check"></i></a></li>
				<li class="nav-item"><a class="nav-link" href="#">買い物カゴ<i
						class="fas fa-cart-arrow-down"></i></a></li>
				<li class="nav-item"><a class="nav-link" href="#">ログアウト<i
						class="fas fa-sign-out-alt"></i>
				</a></li>
			</ul>
		</div>
	</nav>
	<div class="box_big_pmRegist">
		<div class="center-align">
			<h5>購入確認画面</h5>
		</div>
		<div class="box_small_pmRegist">
			<!--  購入履歴 -->
			<div class="center-align">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col"></th>
							<th scope="col">商品名</th>
							<th scope="col">単価</th>
							<th scope="col">小計</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>オリオン</td>
							<td>沖縄</td>
							<td>300円</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>キリン</td>
							<td>神奈川</td>
							<td>300円</td>
						</tr>
						<tr>
							<th scope="row">3</th>
							<td>アサヒ</td>
							<td>東京</td>
							<td>300円</td>
						</tr>
						<tr>
							<th scope="row"></th>
							<td></td>
							<td>配送方法選択</td>
							<td><select name="pref_id">
									<option value="" selected>通常配送</option>
									<option value="1">クール便</option>
							</select></td>
						</tr>
					</tbody>
				</table>
                <p class="center-align">
                    <button type="button" class="btn btn-dark">購入する</button>
                </p>
			</div>
		</div>
	</div>
</body>
</html>
