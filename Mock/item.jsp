<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <meta name=”description” content=”ログイン画面・都道府県から検索できる、クラフトビール（地ビール）オンラインショップです。”/><!--メタデスクリプション-->
        <link href="style.css" rel="stylesheet" type="text/css" /><!--CSSの読み込み-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><!--bootstrap-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <h1 class="head_pm">CRAFT-Bee <i class="fas fa-beer"></i></h1>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"         aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#"> メンバー情報<i class="fas fa-user-check"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> 買い物カゴ<i class="fas fa-cart-arrow-down"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> ログアウト<i
					class="fas fa-sign-out-alt"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
        <div class="row">
				<div class="col s5">
					<div class="card">
						<div class="card-image">
							<img src="item1.jpg">
						</div>
					</div>
				</div>
				<div class="col s5">
					<h4><p class="left-align"><i class="fas fa-beer"></i> 商品名</p>
                                    <p class="left-align"><label>　常陸野ネスト ホワイトエール 木内酒造</label></h4>
                    <div class="form-row">
                                        <!-- 生産地名エリア -->
                                        <div class="form-group col-md-6">
                                            <h5><p class="left-align"><label>　生産地 : 茨城</label></h5>
                                        </div>
                                        <!-- 価格エリア -->
                                        <div class="form-group col-md-6">
                                            <h5><p class="left-align"><label>（消費税込）400円</label></h5>
                                        </div>
                    </div>
                    <p class="left-align"><i class="fas fa-beer"></i> 詳細文</p>
                    <p class="left-align"><label>　奥深い味わいの小麦麦芽とスパイシーなホップ。さらにコリアンダー、オレンジピール、ナツメグ等のスパイスを加えて醸造するベルギーの伝統的ビール、ベルジャンホワイトエールのスタイルで造ったうすにごり小麦ビールです。スパイスの豊かな香りと小麦特有の軽い酸味があいまって様々な料理にも相性よくお飲みいただけます。</label>
                    <form action="ItemAdd" method="POST">
                        <button type="button" class="btn btn-dark">　買い物カゴに追加 <i class="fas fa-cart-arrow-down"></i> 　</button>
					</form>               
				</div>
            </div>
        </div>
    </body>
</html>
